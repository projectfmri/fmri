"""
Developed by :
Zahra Jalili and Demy van de Veen

"""
from nilearn import datasets
from nilearn.connectome import ConnectivityMeasure
from nilearn.input_data import NiftiMapsMasker
import matplotlib.pyplot as plt
from nilearn import plotting
import networkx as nx
import numpy as np


def n_max_values(matrix, n):
    "find index position of n largest values in the matrix"
    idx = np.argpartition(-matrix.ravel(), n)[:n]
    return np.column_stack(np.unravel_index(idx, matrix.shape))


def binarize_matrix(matrix, indices):
    "set the index positions in indices to zero"
    thresholded_matrix = np.zeros(matrix.shape)
    thresholded_matrix[tuple(indices.T)] = 1
    return thresholded_matrix


def threshold_matrix(correlation_matrix):
    "threshold the matrix to only use 25 percent of the edges"
    matrix = np.array(correlation_matrix)
    n = matrix.shape
    number_of_values = n[0] * n[1]
    percent25 = round(0.25 * number_of_values)
    max_25percent = n_max_values(matrix, percent25)
    new_matrix = binarize_matrix(matrix, max_25percent)
    return new_matrix


def network_measures(matrix):
    "calculate local efficiency, global efficiency and clustering coefficient"
    G = nx.from_numpy_matrix(matrix)
    g_eff = nx.global_efficiency(G)
    l_eff = nx.local_efficiency(G)
    clust = nx.average_clustering(G)
    return g_eff, l_eff, clust


def barplot(g_eff1, l_eff1, clust1, g_eff2, l_eff2, clust2):
    "generate a barplot"
    X = np.arange(3)
    fig = plt.figure()
    plt.bar(X, [g_eff1, l_eff1, clust1], width=0.25, label='Session 1')
    plt.bar(X + 0.25, [g_eff2, l_eff2, clust2], width=0.25, label='Session 2')
    plt.xticks(X + 0.25 / 2, ('Global efficiency', 'Local efficiency', 'Clustering coefficient'))
    plt.ylabel('Scores')
    plt.title('Network measures')
    plt.legend()
    plt.show()


def load_dataset():
    atlas_data = datasets.fetch_atlas_msdl()
    atlas_filename = atlas_data.maps
    labels = atlas_data['labels']
    return atlas_data, atlas_filename, labels


def show_time_series(atlas_filename, nifti_path_1, nifti_path_2):
    # fmri_filenames = atlas_data.func[0]
    masker2 = NiftiMapsMasker(maps_img=atlas_filename, standardize=True)
    time_series2_1 = masker2.fit_transform(
        nifti_path_1)
    time_series2_2 = masker2.fit_transform(
        nifti_path_2)
    plt.plot(time_series2_1[:, :2], label="The First Two Voxels for sub149_01")
    plt.plot(time_series2_2[:, :2], label="The First Two Voxels for sub149_02")
    plt.title("Timeseries for Two arbitrary region")
    plt.xlabel("time")
    plt.ylabel("Units of The Brain")
    plt.legend()
    plt.show()
    # print(time_series2.shape)
    return time_series2_1, time_series2_2


def show_correlation_matrix(time_series2_1, time_series2_2, labels):
    correlation_measure2 = ConnectivityMeasure(kind='correlation')
    correlation_matrix2_1 = correlation_measure2.fit_transform([time_series2_1])[0]
    correlation_matrix2_2 = correlation_measure2.fit_transform([time_series2_2])[0]
    np.fill_diagonal(correlation_matrix2_1, 0)
    plotting.plot_matrix(correlation_matrix2_1, figure=(10, 8), labels=labels,
                         vmax=0.8, vmin=-0.8, reorder=True, title="Correlation matrix_sub149_01")
    # reorder false if not the label from sub to sub s diff
    np.fill_diagonal(correlation_matrix2_2, 0)
    plotting.plot_matrix(correlation_matrix2_2, figure=(10, 8), labels=labels,
                         vmax=0.8, vmin=-0.8, reorder=True, title="Correlation matrix_sub149_02")
    plotting.show()
    return correlation_matrix2_1, correlation_matrix2_2


def show_connectome_graph(atlas_data, correlation_matrix2_1, correlation_matrix2_2):
    # display the corresponding graph
    # We threshold to keep only the 20% of edges with the highest value
    # because the graph is very dense
    plotting.plot_connectome(correlation_matrix2_1, atlas_data["region_coords"], edge_threshold="75%", colorbar=True,
                             display_mode="lzr", title="sub149_01")
    plotting.plot_connectome(correlation_matrix2_2, atlas_data["region_coords"], edge_threshold="75%", colorbar=True,
                             display_mode="lzr", title="sub149_02")
    plotting.show()


def threshold_correlation_matrix(atlas_data, correlation_matrix, title):
    new_matrix = threshold_matrix(correlation_matrix)
    plotting.plot_matrix(new_matrix, figure=(10, 8), labels=atlas_data.labels,
                         vmax=0.8, vmin=-0.8, title=title)
    plotting.show()
    return new_matrix


def threshold_connectom(atlas_data, new_matrix, title):
    network_measures(new_matrix)
    plotting.plot_connectome(new_matrix, atlas_data["region_coords"], title=title)
    plotting.show()


def run(nifti_path_1, nifti_path_2):
    atlas_data, atlas_filename, labels = load_dataset()
    time_series2_1, time_series2_2 = show_time_series(atlas_filename, nifti_path_1, nifti_path_2)
    correlation_matrix2_1, correlation_matrix2_2 = show_correlation_matrix(time_series2_1, time_series2_2, labels)
    show_connectome_graph(atlas_data, correlation_matrix2_1, correlation_matrix2_2)
    new_matrix1 = threshold_correlation_matrix(atlas_data, correlation_matrix2_1,
                                               title="correlation threshold_sub149_01")
    g_eff1, l_eff1, clust1 = network_measures(new_matrix1)
    threshold_connectom(atlas_data, new_matrix1, title=" connectom threshold sub149_01")
    new_matrix2 = threshold_correlation_matrix(atlas_data, correlation_matrix2_2,
                                               title="correlation threshold_sub149_02")
    g_eff2, l_eff2, clust2 = network_measures(new_matrix2)
    threshold_connectom(atlas_data, new_matrix2, title=" connectom threshold sub149_02")
    barplot(g_eff1, l_eff1, clust1, g_eff2, l_eff2, clust2)


# if __name__ == "__main__":
#     example how to run the data analysis
#     nifti_file_path_1 = "bids/sourcedata/sub-149/ses-01/func/wsfsub-149_task-rest_run-01_bold.nii.gz"
#     nifti_file_path_2 = "bids/sourcedata/sub-149/ses-01/func/wsfsub-149_task-rest_run-02_bold.nii.gz"
#     run(nifti_file_path_1, nifti_file_path_2)
