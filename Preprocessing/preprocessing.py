
from os.path import join as opj
from nipype.interfaces.io import SelectFiles, DataSink
from nilearn.plotting import plot_anat
from nilearn.plotting import plot_stat_map
from nilearn import image
from IPython.display import Image, display
from nipype.interfaces.fsl import BET, SliceTimer, FLIRT, MCFLIRT, FNIRT, ApplyWarp
from nipype import Workflow, Node
from nipype.interfaces.afni import TProject
from nipype.interfaces.utility import IdentityInterface


class Workf:
    "class of the workflow"
    def __init__(self, name, base_dir):
        "initialize workf class"
        self.workf = Workflow(name=name, base_dir=base_dir)


    def selectfiles(self, bidsdir, template):
        "select the files on which the pipeline is run"
        session_list = ['01', '02']
        infosource = Node(IdentityInterface(fields=['session_name']),
                          name = "infosource")
        infosource.iterables = [('session_name', session_list)]

        anat_file = opj(bidsdir, "ses-{session_name}/anat/sub-*_ses-{session_name}_T1w.nii.gz")
        func_file = opj(bidsdir, "ses-{session_name}/func/sub-*_ses-{session_name}_task-*_bold.nii.gz")
        template = template

        templates = {'anat': anat_file,
                     'func': func_file,
                     'temp': template}
        selectfiles = Node(SelectFiles(templates,
                                       base_directory='.'),
                           name="selectfiles")
        self.workf.connect([(infosource, selectfiles, [("session_name", "session_name")])])
        return selectfiles

    def create_workflow(self, selectfiles, TR, bandpasslow, bandpasshigh, smooth):
        "add all preprocessing steps the workflow and output the resulting images to the datasink"
        bet_anat = Node(BET(frac=0.5,
                            robust=True,
                            output_type='NIFTI_GZ'),
                        name="bet_anat")

        self.workf.connect([(selectfiles, bet_anat, [('anat', 'in_file')])])

        mcflirt = Node(MCFLIRT(mean_vol=True,
                               save_plots=True,
                               output_type='NIFTI'),
                       name="mcflirt")

        self.workf.connect([(selectfiles, mcflirt, [('func', 'in_file')])])

        slicetimer = Node(SliceTimer(index_dir=False,
                                     interleaved=True,
                                     output_type='NIFTI',
                                     time_repetition=TR),
                          name="slicetimer")

        self.workf.connect([(mcflirt, slicetimer, [('out_file', 'in_file')])])

        coreg = Node(FLIRT(
            dof=6,
            cost='mutualinfo',
            out_file="registeredimage.nii.gz",
            out_matrix_file="coregisteredtoanat.mat"),
            name="coreg")

        self.workf.connect([(bet_anat, coreg, [('out_file', 'reference')]),
                         (mcflirt, coreg, [('mean_img', 'in_file')])
                         ])

        normalize_linear = Node(FLIRT(
            dof=12,
            out_matrix_file="normalizedtomni.mat",
            out_file="normalizedimage.nii.gz"),
            name="normalize_linear")

        normalize_nonlinear = Node(FNIRT(
            config_file='T1_2_MNI152_2mm',
            warped_file="normalizednonlinear.nii.gz",
            fieldcoeff_file="T1toMNI_coef.nii.gz",
            field_file="T1toMNI_warp.nii.gz"),
            name="normalize_nonlinear")

        self.workf.connect([(bet_anat, normalize_linear, [('out_file', 'in_file')]),
                         (normalize_linear, normalize_nonlinear, [('out_matrix_file', 'affine_file')]),
                         (selectfiles, normalize_nonlinear, [('anat', 'in_file')]),
                         (selectfiles, normalize_linear, [('temp', 'reference')]),
                         (selectfiles, normalize_nonlinear, [('temp', 'ref_file' )])
                         ])


        applywarp = Node(ApplyWarp(
            out_file="normresult.nii.gz"),
            name="applywarp")

        self.workf.connect([(slicetimer, applywarp, [('slice_time_corrected_file', 'in_file')]),
                         (coreg, applywarp, [('out_matrix_file', 'premat')]),
                         (normalize_nonlinear, applywarp, [('fieldcoeff_file', 'field_file')]),
                          (selectfiles, applywarp, [('temp', 'ref_file')])
                         ])


        filtersmooth = Node(TProject(
            bandpass = (bandpasslow, bandpasshigh),
            blur = smooth,
            out_file = "preprocessed.nii.gz"),
        name = "filtersmooth"
        )

        self.workf.connect([(applywarp, filtersmooth, [('out_file', 'in_file')])])

        datasink = Node(DataSink(base_directory=self.workf.base_dir,
                                 container="datasink"),
                        name="datasink")

        self.workf.connect([(normalize_nonlinear, datasink, [('warped_file',
                                                           'normalize_nonlinear.@warped_file')]),
                         (normalize_linear, datasink, [('out_file',
                                                        'normalize_linear.@out_file')]),
                         (coreg, datasink, [('out_file',
                                             'coreg.@out_file')]),
                            (filtersmooth, datasink, [('out_file', 'filtersmooth.@out_file')])
                         ])

    def run_workflow(self):
        "run the workflow"
        self.workf.run('MultiProc', plugin_args={'n_procs': 8})


def plot_anat(anat):
    " plot anatomical image"
    plot_anat(anat, title='original',
              display_mode='ortho', dim=-1, draw_cross=False, annotate=False);

def plot_statmap(func,anat, slice):
    "plot a statistical map of the functional image on top of the anatomical image"
    plot_stat_map(
        image.index_img(func, slice),
        title='Coregistration', bg_img=anat,
        threshold=10000, display_mode='ortho');

def visualize_graph(workflow):
    'visualize graph of the steps in the workflow'
    workflow.write_graph(graph2use='colored', format='png', simple_form=True)
    display(Image(filename=opj(workflow.base_dir, workflow.name, 'graph.png')))



def run(name, outputdir,TR,subdir, template, bandpasslow, bandpasshigh, smooth):
    "create workflow object and run entire workflow"
    work = Workf(name, outputdir)
    files = work.selectfiles(subdir, template)
    work.create_workflow(files, TR, bandpasslow, bandpasshigh, smooth)
    work.run_workflow()


# if __name__ == '__main__':
#     "example how to run the preprocessing pipeline"
#     name = "preproc2"
#     outputdir = '/home/demy/PycharmProjects/fmripycharm'
#     TR = 2
#     bidsdir = '/home/demy/PycharmProjects/fmripycharm/sub-149'
#     template = '/usr/local/fsl/data/standard/MNI152_T1_2mm_brain.nii.gz'
#     bandpasslow = 0.1
#     bandpasshigh = 10
#     smooth = 4
#     run(name, outputdir,TR, bidsdir, template, bandpasslow, bandpasshigh, smooth)