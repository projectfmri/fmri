# Standardized resting state fMRI pipeline for the Cognitive Neuroscience Center of the UMCG

To analyze rs-fMRI images, a great number of comprehensive software packages were developed,
such as the FMRIB Software Library (FSL), Statistical Parametric Mapping (SPM), Analysis Of Functional Images (AFNI), and Freesurfer [8].
These packages all provide different functions, methods and graphical user interfaces for the analysis of rs-fMRI images.
Nowadays, researchers at the Cognitive NeuroScience Center (CNC) of the University of Groningen write their own analysis scripts using these
different software packages whereby the analysis of rs-fMRI remains rather specific between users.
This leads to duplication of effort and makes it complicated for the researchers to share their data and findings.
Therefore, we developed a standardized pipeline for the analysis of resting state fMRI datasets including a Graphical interface
to run the pipeline.

## Software Requirements
- Numpy
- Matplotlib
- Nipype
- Nilearn
- Networkx
- IPython
- PyQt5
- FSL
- AFNI

You can install the packages Numpy, Matplotlib, Nipype, Nilearn, Networkx and IPython using the following command:

```
pip3 install numpy matplotlib nipype nilearn networkx ipython PyQt5
```

For the installation of FSL we refer to the the website https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation


For the installation of AFNI we refer to https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html



## Running the pipeline
To run the pipeline on rs-fMRI images, start the Graphical User Interface and follow those instructions
- Run (from terminal) $ python3 gui.py
- Choose 3 request input paths(2 dirs and 1 file) and output directory.
- Set parameters TR, Bandpass and Smooth
- Push "Run pipeline" button (bottom right) 

## Authors
- Zahra Jalili
- Demy van de Veen
- Kees Verheule